import sys, getopt
from Client import Client
from FileIo import FileIO


def new_client():
    print("Let ut create a new client!")
    name = input("Client name: ")
    if Client.check_client_exists(name):
        print("Client already exists!")
        exit()
    label = input("Label: ")
    rate = input("Rate (default: 900:- SEK/hour): ")
    a_client = Client(name, label, rate);
    a_client.save_client()

def main(argv):
    try:
        opts, args = getopt.getopt(argv, "ln", ["list", "new"])
    except getopt.GetoptError:
        print('test.py -l or test.py -n')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-l' or opt == '--list':
            Client.list_clients()
            sys.exit()
        elif opt == '-n' or opt == '--new':
            new_client()
            sys.exit()

if __name__ == '__main__':
    main(sys.argv[1:])