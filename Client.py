import os
import settings
from FileIo import FileIO


class Client:
    """Class to handle the clint object"""
    name = ''
    label = ''
    description = ''
    orgnr = ''
    vatnr = ''
    rate = 900

    def __init__(self, name, label, rate=900):
        self.name = name
        self.label = label
        self.rate = rate

    @staticmethod
    def get_clients(names_only=False):
        clients = []
        clients_path = settings.CLIENT_FOLDER
        dir_list = os.listdir(clients_path)
        for client_dir in dir_list:
            client = FileIO.read_yaml_file(clients_path / client_dir / 'client.yml')
            if names_only:
                clients.append(client['client']['name'])
            else:
                clients.append(client['client'])
        return clients

    @staticmethod
    def list_clients():
        clients = Client.get_clients()
        print(clients)

    @staticmethod
    def check_client_exists(name):
        clients = Client.get_clients(True)
        return name in clients

    def save_client(self):
        clients_path = settings.CLIENT_FOLDER
        path = os.path.join(clients_path, self.name)
        FileIO.create_client_folder(path)
        filepath = os.path.join(clients_path, self.name, 'client.yml')
        FileIO.save_yaml_file(filepath, self)
