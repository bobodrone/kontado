import yaml
import settings
import os


class FileIO:
    """File Io class handle input och output of files"""

    @staticmethod
    def read_yaml_file(file_path):
        with open(file_path) as file:
            try:
                yaml_dict = yaml.safe_load(file)
                return yaml_dict
            except yaml.YAMLError as exc:
                print(exc)

    @staticmethod
    def create_client_folder(path):
        os.mkdir(path)

    @staticmethod
    def save_yaml_file(file_path, yaml_dict):
        with open(file_path, 'w') as file:
            try:
                yaml.dump(yaml_dict, file)
            except yaml.YAMLError as exc:
                print(exc)
